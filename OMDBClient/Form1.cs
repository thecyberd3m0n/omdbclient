﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using OMDBClient;
using Newtonsoft.Json.Linq;

namespace OMDBClient
    {
    public partial class myIMDB : Form
        
        {
        private bool connErrors;
        private omdbClass omdbclass;
        private string selected;
        private JArray searchResults;
        string movieJSON;
        private string searchString="";
        public myIMDB()
            {
            InitializeComponent();
            omdbclass = new omdbClass();
            getMovie.WorkerSupportsCancellation = true;
            search.WorkerSupportsCancellation = true;
            }

        private void searchButton_Click(object sender, EventArgs e)
            {
            if (searchString != searchBox.Text.ToString())
                    {
                searchString = searchBox.Text.ToString();
                try
                    {
                    search.RunWorkerAsync();
                    }
                catch (InvalidOperationException)
                    {
                    search.CancelAsync();
                    }
                }
            }

        private void connector_DoWork(object sender, DoWorkEventArgs e)
            {
            updateInfoLabel("Searching...");
            try
                {
                connErrors = false;
                string json_response = omdbclass.GetJSONByName(RemoveAccent(searchBox.Text.ToString()));
                e.Result = json_response;
                }
            catch (Exception)
                {
                updateInfoLabel("Connection problem");
                connErrors = true;
                }
            }

        private static string RemoveAccent(string txt)
            {

            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);

            return System.Text.Encoding.ASCII.GetString(bytes);

            }

        private void connector_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
            if (connErrors == true)
                {
                return;
                }
            JObject o = JObject.Parse(e.Result.ToString());
            if ((JArray) o["Search"] != null)
                {
                searchResults = (JArray) o["Search"];
                resultsDG.Rows.Clear();

                string information =  "Found " + searchResults.Count + " results";
                updateInfoLabel(information);
                for (int i = 0; i < searchResults.Count; i++)
                    {
                    resultsDG.Rows.Add(searchResults[i]["Title"], searchResults[i]["Year"],searchResults[i]["imdbID"]);
                    }
                }
                
                else
                    {
                    infoLabel.Text = "Not Found";
                    }
            selected = searchResults[0]["imdbID"].ToString();
            //search.Dispose();
            //getMovie.RunWorkerAsync();
                
            }
        private void updateInfoLabel (string label)
            {
            Action updateLabel = () => infoLabel.Text = label.ToString();
            infoLabel.Invoke(updateLabel);
            }
        private void exitButton_Click(object sender, EventArgs e)
            {
            Application.Exit();
            }

        private void resultsDG_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
            {
            
            }

        private void getMovie_DoWork(object sender, DoWorkEventArgs e)
            {
            if (getMovie.CancellationPending)
                {
                e.Cancel = true;
                }
            else
                {
                updateInfoLabel("Getting...");
                try
                    {
                    connErrors = false;
                    Debug.Write("imdbID = " + selected);
                    string json_response = omdbclass.GetJSONByID(selected);
                    movieJSON = json_response;
                    }
                catch (Exception)
                    {
                    updateInfoLabel("Connection problem");
                    connErrors = true;
                    }
                }
            }

        private void getMovie_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
            JObject o = JObject.Parse(movieJSON.ToString());
            Debug.Write(o);
            updateInfoLabel("Got " + o["Title"].ToString());
            try
                {
                moviePoster.Load(o["Poster"].ToString());
                }
            catch (Exception)
                {
                moviePoster.Image = null;
                }
            titleLabel.Text = o["Title"].ToString() + " (" + o["Runtime"].ToString() +")";
            genreLabel.Text = o["Genre"].ToString();
            directorLabel.Text = o["Director"].ToString();
            writerLabel.Text = o["Writer"].ToString();
            actorsLabel.Text = o["Actors"].ToString();
            plotLabel.Text = o["Plot"].ToString();
            ratingLabel.Text = o["imdbRating"].ToString()+"/10";
            getMovie.Dispose();
            }

        private void resultsDG_SelectionChanged(object sender, EventArgs e)
            {
            int row = resultsDG.CurrentRow.Index;
            DataGridViewRow selectedRow = resultsDG.Rows[row];
            selected = Convert.ToString(selectedRow.Cells["imdbID"].Value);
            getMovie.CancelAsync();
            if (getMovie.IsBusy != true)
                {
                getMovie.RunWorkerAsync();
                }
            else
                {
                getMovie.CancelAsync();

                }
            }
        }
    }
