﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Diagnostics;
using System.Threading;
using System.Web;

namespace OMDBClient
    {
    class omdbClass
        {
        public omdbClass()
            {
            
            }

        public string GetJSONByName(string query)
            {
            string result;
            try
                {
                string url = "http://www.omdbapi.com?s=" + HttpUtility.UrlEncode(query);

                Debug.Write(url);
                result = new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
                }
            catch (HttpRequestException)
                {
                throw new Exception("Connection problem");
                }
            return result;
            }

        public string GetJSONByID(string id)
            {
            string result;
            try
                {
                string url = "http://www.omdbapi.com?i=" + id;
                Debug.Write(url);
                result =  new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
                }
            catch (HttpRequestException)
                {
                throw new Exception("Connection problem");
                }
            return result;
            }
        }
    }
